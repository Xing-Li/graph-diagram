import React, { Component } from 'react';
import {
  ArrayLike,
  selection,
  select,
  selectAll,
  Selection,
  event,
} from "d3-selection";
import * as d3Zoom from "d3-zoom";

import {
  Diagram,
  Markup,
  Model,
  ModelToCypher,
  ModelToD3,
  Node,
  Relationship,
  Scaling,
  LayoutModel,
  LayoutNode,
} from "Components/Diagram";
class ExtractBrowser extends Component {
  constructor(props) {
    super(props);
    const { currentLabel, newRelationshipName, newNodeName } = this.props;
    let exampleMarkup = `<ul class="graph-diagram-markup" data-internal-scale="1" data-external-scale="1">
    <li class="node" data-node-id="1" data-x="435" data-y="100">
      <span class="caption">${newNodeName}</span><dl class="properties"></dl></li>
    <li class="node" data-node-id="0" data-x="135" data-y="100">
      <span class="caption">${currentLabel}</span><dl class="properties"></dl></li>
    <li class="relationship" data-from="1" data-to="0">
      <span class="type">${newRelationshipName}</span><dl class="properties"></dl></li>
  </ul>
  `;
    this.save(exampleMarkup);
    this.state = {
      graphModel: null,
      svg: null,
      newRelationship: null,
      diagram: null,
      cacheNodes: [],
      cacheRelationShips: []
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentLabel, newRelationshipName, newNodeName } = this.props;
    const { graphModel, cacheNodes, cacheRelationShips } = this.state;
    if (graphModel && cacheNodes.length) {
      let propsKeys = ["currentLabel", "newNodeName"];
      for (let loo = 0; loo < propsKeys.length; loo++) {
        let propsKey = propsKeys[loo];
        if (prevProps[propsKey] != this.props[propsKey]) {
          for (let index = 0; index < cacheNodes.length; index++) {
            let node = cacheNodes[index].model;
            if (node.caption === prevProps[propsKey]) {
              node.caption = this.props[propsKey];
              this.save(this.formatMarkup());
              this.draw();
              break;
            }
          }
        }
      }
    }
    if (graphModel && cacheRelationShips.length) {
      if (prevProps.newRelationshipName != this.props.newRelationshipName) {
        for (let index = 0; index < cacheRelationShips.length; index++) {
          let node = cacheRelationShips[index].model;
          if (node.relationshipType === prevProps.newRelationshipName) {
            node.relationshipType = this.props.newRelationshipName;
            this.save(this.formatMarkup());
            this.draw();
            break;
          }
        }
      }
    }
  }
  getLocalStorageGraphName() {
    const graphName = "example";
    return `graph-diagram-markup-${graphName}`;
  }

  formatMarkup() {
    const { graphModel } = this.state;
    let container = select("body").append("div");
    Markup.format(graphModel, container);
    let markup = container.node().innerHTML;
    markup = markup
      .replace(/<li/g, "\n  <li")
      .replace(/<span/g, "\n    <span")
      .replace(/<\/span><\/li/g, "</span>\n  </li")
      .replace(/<\/ul/, "\n</ul");
    container.remove();
    return markup;
  }

  draw() {
    const { graphModel,
      diagram,
      svg } = this.state;
    if (graphModel && diagram && diagram.render) {
      svg.data([graphModel]).call(diagram.render);
    }
  }

  save(markup) {
    localStorage.setItem(this.getLocalStorageGraphName(), markup);
  }

  componentDidMount() {
    let svg, graphModel;
    if (svg) {
      select("svg").remove();
      svg = null;
    }
    svg = select("#svgContainer")
      .append("svg:svg")
      .attr("class", "graphdiagram")
      .attr("id", "svgElement")
      .attr("width", "100%")
      .attr("height", "100%")
      // .call(
      //   d3Zoom.zoom().on("zoom", function () {
      //     svg.attr("transform", event.transform);
      //   })
      // )
      .on("dblclick.zoom", null)
      .append("g");

    let svgElement = document.getElementById("svgElement");
    let x = svgElement.clientWidth / 2;
    let y = svgElement.clientHeight / 2;
    let w = 10,
      h = 10,
      s = "#999999",
      so = 0.5,
      sw = "1px";
    svg
      .append("line")
      .attr("x1", x - w / 2)
      .attr("y1", y)
      .attr("x2", x + w / 2)
      .attr("y2", y)
      .style("stroke", s)
      .style("stroke-opacity", so)
      .style("stroke-width", sw);
    svg
      .append("line")
      .attr("x1", x)
      .attr("y1", y - h / 2)
      .attr("x2", x)
      .attr("y2", y + h / 2)
      .style("stroke", s)
      .style("stroke-opacity", so)
      .style("stroke-width", sw);
    let newNode;
    if (!localStorage.getItem(this.getLocalStorageGraphName())) {
      graphModel = new Model();
      newNode = graphModel.createNode();
      newNode.x = svgElement.clientWidth / 2;
      newNode.y = svgElement.clientHeight / 2;
      thiz.save(thiz.formatMarkup());
    } else {
      graphModel = parseMarkup(localStorage.getItem(this.getLocalStorageGraphName()));
    }
    function parseMarkup(markup) {
      let container = select("body").append("div");
      container.node().innerHTML = markup;
      let model = Markup.parse(container.select("ul.graph-diagram-markup"), null);
      container.remove();
      return model;
    }
    this.setState({ graphModel: graphModel, svg: svg }, () => {
      this.draw();
      this.setState({
        diagram: new Diagram()
          // .scaling(Scaling.centerOrScaleDiagramToFitSvg)
          .scaling(null)
      }, () => {
        this.init();
      })
    })
  }
  init() {
    const { currentLabel } = this.props;
    const {
      diagram,
    } = this.state;
    let thiz = this;
    diagram.overlay(function (layoutModel, view) {
      let nodeOverlays = view
        .selectAll("circle.node.overlay")
        .data(layoutModel.nodes);
      thiz.setState({
        cacheNodes: layoutModel.nodes,
        cacheRelationShips: layoutModel.relationships
      });
      for (let index; index < layoutModel.nodes.length; index++) {
        console.log(layoutModel.nodes[index].caption);
      }
      nodeOverlays.exit().remove();
      let nodeOverlaysEnter = nodeOverlays
        .enter()
        .append("circle")
        .attr("class", "node overlay");
      let merge = nodeOverlays.merge(nodeOverlaysEnter);
      merge
        .on("dblclick", editNode)
        .attr("r", function (node) {
          return node.radius.outside();
        })
        .attr("stroke", "none")
        .attr("fill", "rgba(255, 255, 255, 0)")
        .attr("cx", function (node) {
          let graphNode = node.model;
          return graphNode.ex();
        })
        .attr("cy", function (node) {
          let graphNode = node.model;
          return graphNode.ey();
        });
      let nodeRings = view.selectAll("circle.node.ring").data(layoutModel.nodes);
      nodeRings.exit().remove();
      let nodeRingsEnter = nodeRings
        .enter()
        .append("circle")
        .attr("class", "node ring");
      merge = nodeRings.merge(nodeRingsEnter);
      merge
        .on("dblclick", editNode)
        .attr("r", function (node) {
          return node.radius.outside() + 5;
        })
        .attr("fill", "none")
        .attr("stroke", "rgba(255, 255, 255, 0)")
        .attr("stroke-width", "10px")
        .attr("cx", function (node) {
          let graphNode = node.model;
          return graphNode.ex();
        })
        .attr("cy", function (node) {
          let graphNode = node.model;
          return graphNode.ey();
        });
      let relationshipsOverlays = view
        .selectAll("path.relationship.overlay")
        .data(layoutModel.relationships);
      relationshipsOverlays.exit().remove();
      let relationshipsOverlaysEnter = relationshipsOverlays
        .enter()
        .append("path")
        .attr("class", "relationship overlay");
      merge = relationshipsOverlays.merge(relationshipsOverlaysEnter);
      merge
        .attr("fill", "rgba(255, 255, 255, 0)")
        .attr("stroke", "rgba(255, 255, 255, 0)")
        .attr("stroke-width", "10px")
        .on("dblclick", editRelationship)
        .attr("transform", function (r) {
          let angle = r.start.model.angleTo(r.end.model);
          return (
            "translate(" +
            r.start.model.ex() +
            "," +
            r.start.model.ey() +
            ") rotate(" +
            angle +
            ")"
          );
        })
        .attr("d", function (d) {
          return d.arrow.outline;
        });
    });

    function getGraphList() {
      let localStorageItemName;
      let graphList = [];
      for (localStorageItemName in localStorage) {
        if (localStorageItemName.indexOf("graph-diagram-markup-") >= 0) {
          let parts = localStorageItemName.split("graph-diagram-markup-");
          let graphName = parts[1];
          graphList.push(graphName);
        }
      }
      return graphList;
    }

    function onControlEnter(saveChange) {
      return function () {
        if (event.ctrlKey && event.keyCode === 13) {
          saveChange();
        }
      };
    }

    function editNode() {
      let node = this.__data__.model;
      if (currentLabel === node.caption) return;
      let editor = select(".pop-up-editor.node");
      appendModalBackdrop();
      editor.classed("hide", false);
      let captionField = editor.select("#node_caption");
      captionField.node().value = node.caption || "";
      captionField.node().select();
      function saveChange() {
        node.caption = captionField.node().value;
        thiz.save(thiz.formatMarkup());
        thiz.draw();
        cancelModal();
      }
      captionField.on("keypress", onControlEnter(saveChange));
      editor.select("#edit_node_save").on("click", saveChange);
    }

    function editRelationship() {
      let editor = select(".pop-up-editor.relationship");
      appendModalBackdrop();
      editor.classed("hide", false);
      let relationship = this.__data__.model;
      let relationshipTypeField = editor.select("#relationship_type");
      relationshipTypeField.node().value = relationship.relationshipType || "";
      relationshipTypeField.node().select();
      function saveChange() {
        relationship.relationshipType = relationshipTypeField.node().value;
        thiz.save(thiz.formatMarkup());
        thiz.draw();
        cancelModal();
      }
      relationshipTypeField.on("keypress", onControlEnter(saveChange));
      editor.select("#edit_relationship_save").on("click", saveChange);
    }

    function cancelModal() {
      selectAll(".pop-modal").classed("hide", true);
      selectAll(".modal-backdrop").remove();
    }

    selectAll(".btn.cancel").on("click", cancelModal);
    selectAll(".pop-modal").on("keyup", function () {
      if (event.keyCode === 27) cancelModal();
    });

    function appendModalBackdrop() {
      select("body")
        .append("div")
        .attr("class", "modal-backdrop")
        .on("click", cancelModal);
    }

    select(window).on("resize", function () { thiz.draw() });

    selectAll(".modal-dialog").on("click", function () {
      event.stopPropagation();
    });

    let graphDatalist = select("#graphlist");
    graphDatalist
      .selectAll("option")
      .data(getGraphList)
      .enter()
      .append("option")
      .attr("value", function (d) {
        return d;
      });

    thiz.draw();
  }

  render() {
    return (<div>
      <div id="svgContainer">
        <div className="pop-modal hide pop-up-editor node" tabIndex="-1">
          <input id="node_caption" type="text" defaultValue="A" />
          <div className="btn save" id="edit_node_save">save</div>
          <div className="btn cancel">cancel</div>
        </div>
        <div className="pop-modal hide pop-up-editor relationship" tabIndex="-1">
          <input id="relationship_type" type="text" defaultValue="R" />
          <div className="btn save" id="edit_relationship_save">save</div>
          <div className="btn cancel">cancel</div>
        </div></div>
    </div>)
  }
}
export default ExtractBrowser