import {
  ArrayLike,
  selection,
  select,
  selectAll,
  Selection,
  event,
} from "d3-selection";
import * as d3Zoom from "d3-zoom";

import {
  Diagram,
  Markup,
  Model,
  ModelToCypher,
  ModelToD3,
  Node,
  Relationship,
  Scaling,
  LayoutModel,
  LayoutNode,
} from "../src/index";

var graphModel: Model;
var newNode: Node = null;
var newRelationship: Relationship = null;
var graphName: string = "example";
var exampleMarkup: string = `<ul class="graph-diagram-markup" data-internal-scale="1" data-external-scale="1">
      <li class="node" data-node-id="0" data-x="438" data-y="270">
        <span class="caption">Food</span><dl class="properties"></dl></li>
      <li class="node" data-node-id="1" data-x="200" data-y="270">
        <span class="caption">Pizza</span><dl class="properties"></dl></li>
      <li class="relationship" data-from="1" data-to="0">
        <span class="type">IS_A</span><dl class="properties"></dl></li>
    </ul>
    `;
save(exampleMarkup);
loadGraph();
var svg: any;
var diagram = new Diagram()
  // .scaling(Scaling.centerOrScaleDiagramToFitSvg)
  .scaling(null)
  .overlay(function (layoutModel: LayoutModel, view: any) {
    var nodeOverlays = view
      .selectAll("circle.node.overlay")
      .data(layoutModel.nodes);
    nodeOverlays.exit().remove();
    var nodeOverlaysEnter = nodeOverlays
      .enter()
      .append("circle")
      .attr("class", "node overlay");
    var merge = nodeOverlays.merge(nodeOverlaysEnter);
    merge
      .on("dblclick", editNode)
      .attr("r", function (node: LayoutNode) {
        return node.radius.outside();
      })
      .attr("stroke", "none")
      .attr("fill", "rgba(255, 255, 255, 0)")
      .attr("cx", function (node: LayoutNode) {
        let graphNode: Node = node.model as Node;
        return graphNode.ex();
      })
      .attr("cy", function (node: LayoutNode) {
        let graphNode: Node = node.model as Node;
        return graphNode.ey();
      });
    var nodeRings = view.selectAll("circle.node.ring").data(layoutModel.nodes);
    nodeRings.exit().remove();
    var nodeRingsEnter = nodeRings
      .enter()
      .append("circle")
      .attr("class", "node ring");
    var merge = nodeRings.merge(nodeRingsEnter);
    merge
      .attr("r", function (node: LayoutNode) {
        return node.radius.outside() + 5;
      })
      .attr("fill", "none")
      .attr("stroke", "rgba(255, 255, 255, 0)")
      .attr("stroke-width", "10px")
      .attr("cx", function (node: LayoutNode) {
        let graphNode: Node = node.model as Node;
        return graphNode.ex();
      })
      .attr("cy", function (node: LayoutNode) {
        let graphNode: Node = node.model as Node;
        return graphNode.ey();
      });
    var relationshipsOverlays = view
      .selectAll("path.relationship.overlay")
      .data(layoutModel.relationships);
    relationshipsOverlays.exit().remove();
    var relationshipsOverlaysEnter = relationshipsOverlays
      .enter()
      .append("path")
      .attr("class", "relationship overlay");
    var merge = relationshipsOverlays.merge(relationshipsOverlaysEnter);
    merge
      .attr("fill", "rgba(255, 255, 255, 0)")
      .attr("stroke", "rgba(255, 255, 255, 0)")
      .attr("stroke-width", "10px")
      .on("dblclick", editRelationship)
      .attr("transform", function (r: any) {
        var angle = r.start.model.angleTo(r.end.model);
        return (
          "translate(" +
          r.start.model.ex() +
          "," +
          r.start.model.ey() +
          ") rotate(" +
          angle +
          ")"
        );
      })
      .attr("d", function (d: any) {
        return d.arrow.outline;
      });
  });

function getLocalStorageGraphName(): string {
  return `graph-diagram-markup-${graphName}`;
}

var localStorageItemName: string;
var graphList: string[] = [];
function getGraphList(): string[] {
  for (localStorageItemName in localStorage) {
    if (localStorageItemName.indexOf("graph-diagram-markup-") >= 0) {
      var parts: string[] = localStorageItemName.split("graph-diagram-markup-");
      var graphName: string = parts[1];
      graphList.push(graphName);
    }
  }
  return graphList;
}

function draw() {
  if (graphModel && diagram && diagram.render) {
    svg.data([graphModel]).call(diagram.render);
  }
}

function save(markup: any) {
  localStorage.setItem(getLocalStorageGraphName(), markup);
  //localStorage.setItem( "graph-diagram-style", select( "link.graph-style" ).attr( "href" ) );
}

function onControlEnter(saveChange: any) {
  return function () {
    if (event.ctrlKey && event.keyCode === 13) {
      saveChange();
    }
  };
}

function editNode() {
  var editor = select(".pop-up-editor.node");
  appendModalBackdrop();
  editor.classed("hide", false);
  var node = this.__data__.model;
  var captionField: any = editor.select("#node_caption");
  captionField.node().value = node.caption || "";
  captionField.node().select();
  var propertiesField: any = editor.select("#node_properties");
  propertiesField.node().value = node.properties
    .listEditable()
    .reduce(function (previous: string, property: any) {
      return previous + property.key + ": " + property.value + "\n";
    }, "");
  function saveChange() {
    node.caption = captionField.node().value;
    node.properties.clearAll();
    propertiesField
      .node()
      .value.split("\n")
      .forEach(function (line: string) {
        var tokens = line.split(/: */);
        if (tokens.length === 2) {
          var key = tokens[0].trim();
          var value = tokens[1].trim();
          if (key.length > 0 && value.length > 0) {
            node.properties.set(key, value);
          }
        }
      });
    save(formatMarkup());
    draw();
    cancelModal();
  }

  function deleteNode() {
    graphModel.deleteNode(node);
    save(formatMarkup());
    draw();
    cancelModal();
  }

  captionField.on("keypress", onControlEnter(saveChange));
  propertiesField.on("keypress", onControlEnter(saveChange));
  editor.select("#edit_node_save").on("click", saveChange);
  editor.select("#edit_node_delete").on("click", deleteNode);
}

function editRelationship() {
  var editor = select(".pop-up-editor.relationship");
  appendModalBackdrop();
  editor.classed("hide", false);

  var relationship = this.__data__.model;

  var relationshipTypeField: any = editor.select("#relationship_type");
  relationshipTypeField.node().value = relationship.relationshipType || "";
  relationshipTypeField.node().select();

  var propertiesField: any = editor.select("#relationship_properties");
  propertiesField.node().value = relationship.properties
    .listEditable()
    .reduce(function (previous: string, property: any) {
      return previous + property.key + ": " + property.value + "\n";
    }, "");

  function saveChange() {
    relationship.relationshipType = relationshipTypeField.node().value;
    relationship.properties.clearAll();
    propertiesField
      .node()
      .value.split("\n")
      .forEach(function (line: string) {
        var tokens = line.split(/: */);
        if (tokens.length === 2) {
          var key = tokens[0].trim();
          var value = tokens[1].trim();
          if (key.length > 0 && value.length > 0) {
            relationship.properties.set(key, value);
          }
        }
      });
    save(formatMarkup());
    draw();
    cancelModal();
  }

  function reverseRelationship() {
    relationship.reverse();
    save(formatMarkup());
    draw();
    cancelModal();
  }

  function deleteRelationship() {
    graphModel.deleteRelationship(relationship);
    save(formatMarkup());
    draw();
    cancelModal();
  }

  relationshipTypeField.on("keypress", onControlEnter(saveChange));
  propertiesField.on("keypress", onControlEnter(saveChange));
  editor.select("#edit_relationship_save").on("click", saveChange);
  editor.select("#edit_relationship_reverse").on("click", reverseRelationship);
  editor.select("#edit_relationship_delete").on("click", deleteRelationship);
}

function formatMarkup() {
  var container: any = select("body").append("div");
  Markup.format(graphModel, container);
  var markup: any = container.node().innerHTML;
  markup = markup
    .replace(/<li/g, "\n  <li")
    .replace(/<span/g, "\n    <span")
    .replace(/<\/span><\/li/g, "</span>\n  </li")
    .replace(/<\/ul/, "\n</ul");
  container.remove();
  return markup;
}

function cancelModal() {
  selectAll(".modal").classed("hide", true);
  selectAll(".modal-backdrop").remove();
}

selectAll(".btn.cancel").on("click", cancelModal);
selectAll(".modal").on("keyup", function () {
  if (event.keyCode === 27) cancelModal();
});

function appendModalBackdrop() {
  select("body")
    .append("div")
    .attr("class", "modal-backdrop")
    .on("click", cancelModal);
}
function loadGraph() {
  if (svg) {
    select("svg").remove();
    svg = null;
  }

  svg = select("#svgContainer")
    .append("svg:svg")
    .attr("class", "graphdiagram")
    .attr("id", "svgElement")
    .attr("width", "100%")
    .attr("height", "100%")
    .call(
      d3Zoom.zoom().on("zoom", function () {
        svg.attr("transform", event.transform);
      })
    )
    .on("dblclick.zoom", null)
    .append("g");

  var svgElement = document.getElementById("svgElement");
  var x = svgElement.clientWidth / 2;
  var y = svgElement.clientHeight / 2;

  var w = 10,
    h = 10,
    s = "#999999",
    so = 0.5,
    sw = "1px";

  svg
    .append("line")
    .attr("x1", x - w / 2)
    .attr("y1", y)
    .attr("x2", x + w / 2)
    .attr("y2", y)
    .style("stroke", s)
    .style("stroke-opacity", so)
    .style("stroke-width", sw);

  svg
    .append("line")
    .attr("x1", x)
    .attr("y1", y - h / 2)
    .attr("x2", x)
    .attr("y2", y + h / 2)
    .style("stroke", s)
    .style("stroke-opacity", so)
    .style("stroke-width", sw);

  if (!localStorage.getItem(getLocalStorageGraphName())) {
    graphModel = new Model();
    newNode = graphModel.createNode();
    newNode.x = svgElement.clientWidth / 2;
    newNode.y = svgElement.clientHeight / 2;
    save(formatMarkup());
  } else {
    graphModel = parseMarkup(localStorage.getItem(getLocalStorageGraphName()));
  }

  draw();
}

function parseMarkup(markup: any) {
  var container: any = select("body").append("div");
  container.node().innerHTML = markup;
  var model = Markup.parse(container.select("ul.graph-diagram-markup"), null);
  container.remove();
  return model;
}

select(window).on("resize", draw);

selectAll(".modal-dialog").on("click", function () {
  event.stopPropagation();
});

var graphDatalist = select("#graphlist");
graphDatalist
  .selectAll("option")
  .data(getGraphList)
  .enter()
  .append("option")
  .attr("value", function (d) {
    return d;
  });

draw();
