import * as React from "react";
import * as ReactDom from "react-dom";
import ExtractBrowser from "./ExtractBrowser";
class Xss extends React.Component {
    render(){
        return <ExtractBrowser currentLabel="XXX"></ExtractBrowser>
    }
}

ReactDom.render(<Xss />, document.getElementById("react-dom"));